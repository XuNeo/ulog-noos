/**
 * @author Neo Xu (neo.xu1990@gmail.com)
 * @license The MIT License (MIT)
 * 
 * Copyright (c) 2019 Neo Xu
 * Port from rt-thread ulog to use it without rt-thread. License attached below.
*/
/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-09-04     armink       the first version
 */

#include <ulog.h>

#ifdef RT_USING_ULOG
#ifdef ULOG_BACKEND_USING_CONSOLE
static struct ulog_backend console;

#ifdef ULOG_USING_PRINTF
#include <printf.h> //used for printf
#else
#include "stdio.h"
#endif

void ulog_console_backend_output(struct ulog_backend *backend, uint32_t level, const char *tag, bool is_raw,
        const char *log, size_t len)
{
#ifdef ULOG_USING_USH
  printf("ulog ");
#endif
  printf("%s", log);
}

int ulog_console_backend_init(void)
{
    console.output = ulog_console_backend_output;

    ulog_backend_register(&console, "console", true);

    return 0;
}

#endif /* ULOG_BACKEND_USING_CONSOLE */
#endif
